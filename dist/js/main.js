
(function( $ ) {

	var App = function() {

		$('.icon-menu').on('click', $.proxy(this.toggleMenu, this));

		$("#hero").carousel();
		$('#latest-news-slider').carousel();
		$('#inside-focus-slider').carousel();
		$('#recent-movies-slider').carousel();

	};

	App.prototype.toggleMenu = function ( e )
	{
		var $container = $('.main-container');

		if ($container.hasClass('menu-open'))
		{
			$container.removeClass('menu-open');
		}
		else {
			$container.addClass('menu-open');
		}
	}

	new App();

}( jQuery ));