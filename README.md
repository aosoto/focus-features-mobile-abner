Focus Features Mobile Documentation
================

Quick and simple documentation of the build process

## Build Instructions

Dependencies are:

- [Node](http://nodejs.org/)
- [Gulp](http://gulpjs.com/)

#### Install Dependencies

If all of the dependencies have been met, all you have to do is type the following in a Terminal window.

```
$ sudo npm install
```

You may have to install gulp globally

```
$ sudo npm install -g gulp
```

Run the following in the same directory as gulpfile.js to get a local server running

```
$ gulp
```

The server should now be up and running here (http://localhost:1337). This server will be serving the generated files in the dist folder, which are not checked into git.

That's it!